function price()
{
    var re = Number(document.getElementById("q").value);
    let re_1 = document.getElementsByName("t");
    let select_tom = re_1[0];
    let a = 0;
    let b = prices();
    let c = parseInt(select_tom.value) - 1;
    if (c >= 0)
    {
        a = b.types[c];
    }
    let d = document.getElementById("options");
    d.style.display = (select_tom.value == "2" ? "block" : "none");
    let e = document.getElementsByName("options");
    e.forEach(function(radio) 
    {
        if (radio.checked) 
        {
            let f = b.options[radio.value];
            if (f !== undefined && select_tom.value == "2") 
            {
                a += f;
            }
        }
    });
    let g = document.getElementById("pro");
    g.style.display = (select_tom.value == "3" ? "block" : "none");
    let o = document.querySelectorAll("#pro input");
    o.forEach(function(checkbox)
    {
        if (checkbox.checked) 
        {
            let k = b.pro[checkbox.name];
            if (k !== undefined && select_tom.value == "3") 
            {
                a += k;
            }
        }
    });
    if (!(/^(0|[1-9][0-9]*)$/).test(re))
    {
        alert("Вы указали неверный формат (Введите цифры)");
    }
    else
    {
        a *= re;
        document.getElementById("Apay").innerHTML = "Стоимость равна: " + a;
    }
}
function prices()
{
    return {
        types: [1990, 0, 0],
        options:
        {
            One: 1990,
            Two: 2490,
            Three: 2990,
        },
        pro:
        {
            Onet: 3100,
            Twot: 3200,
        }
    };
}
document.addEventListener('DOMContentLoaded', function (event) 
{
    let d = document.getElementById("options");
    d.style.display = "none";
    let re_1 = document.getElementsByName("t");
    let select_tom = re_1[0];
    select_tom.addEventListener("change", function(event) 
    {
        let target = event.target;
        console.log(target.value);
        price();
    });
    let radios = document.getElementsByName("options");
    radios.forEach(function(radio) 
    {
        radio.addEventListener("change", function(event) 
        {
            let r = event.target;
            console.log(r.value);
            price();
        });
    });
    let pre = document.getElementById("pro");
    pre.style.display = "none";
    let checkboxes = document.querySelectorAll("#pro input");
    checkboxes.forEach(function(checkbox) 
    {
        checkbox.addEventListener("change", function(event) 
        {
            let a = event.target;
            console.log(a.name);
            console.log(a.value);
            price();
        });
    });
});
